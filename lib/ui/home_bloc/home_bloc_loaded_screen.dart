import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Data> data;

  const HomeBlocLoadedScreen({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          var series = data[index].series;
          return GestureDetector(
            onTap: () {
              if (series != null) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => detailMovie(series),
                  ),
                );
              } else {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text('Tidak Ada Data'),
                  ),
                );
              }
            },
            child: movieItemWidget(data[index]),
          );
        },
      ),
    );
  }

  Widget movieItemWidget(Data data) {
    return _card(data.i.imageUrl, data.l, data.year.toString());
  }

  Widget detailMovie(series) {
    return Scaffold(
      body: ListView.builder(
        itemCount: series.length,
        itemBuilder: (context, index) {
          return detailItemMovie(series[index]);
        },
      ),
    );
  }

  Widget detailItemMovie(Series series) {
    return _card(series.i.imageUrl, series.l, '');
  }

  Widget _text(text) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
      child: Text(text, textDirection: TextDirection.ltr),
    );
  }

  Widget _card(image, title, year) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(25.0)),
      ),
      child: Column(
        children: [
          Padding(padding: EdgeInsets.all(25), child: Image.network(image)),
          _text(title),
          _text(year)
        ],
      ),
    );
  }
}
