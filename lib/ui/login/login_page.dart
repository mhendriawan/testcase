import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_storage/get_storage.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';

GetStorage dataUserXS = GetStorage();
List dataUser = [];

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _usernameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;
  var type = 'Login';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is AuthBlocLoggedInState) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (context) => HomeBlocCubit()..fetchData(),
                  child: HomeBlocScreen(),
                ),
              ),
            );
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silakan $type terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(height: 9),
                _form(),
                SizedBox(height: 9),
                Center(
                  child: CustomButton(
                    onPressed: (type == 'Login') ? handleLogin : handleRegister,
                    child: Text('$type'),
                  ),
                ),
                _register(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          (type == 'Login')
              ? Container()
              : CustomTextFormField(
                  context: context,
                  controller: _usernameController,
                  isEmail: true,
                  hint: 'hendri',
                  label: 'Username',
                ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
              return val;
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          if (type == 'Login') {
            setState(() {
              type = 'Daftar';
            });
          } else {
            setState(() {
              type = 'Login';
            });
          }
        },
        child: RichText(
          text: TextSpan(
            text: ((type == 'Login') ? 'Belum' : 'Sudah') + ' punya akun? ',
            style: TextStyle(color: Colors.black45),
            children: [
              TextSpan(text: (type == 'Login') ? 'Daftar' : 'Login'),
            ],
          ),
        ),
      ),
    );
  }

  void handleLogin() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    var dtUser;
    var dataEmail;
    var dataPassword;
    if (dataUser != null) {
      var checkEmail = dataUser.where((x) => _email == (x['email']));
      dtUser = checkEmail.toList();

      if (dtUser.length != 0) {
        dataEmail = dtUser[0]['email'];
        dataPassword = dtUser[0]['password'];
      }
    }

    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null &&
        dataEmail == _email &&
        dataPassword == _password) {
      toast('Login Berhasil');
      goHome(_email, _password);
    } else {
      toast('Login Gagal');
    }
  }

  void handleRegister() async {
    final _user = _usernameController.value;
    final _email = _emailController.value;
    final _password = _passwordController.value;
    var checkEmail;
    var lDtUser = 0;
    if (dataUser != null) {
      checkEmail = dataUser.where((x) => _email == (x['email']));
      lDtUser = checkEmail.length;
    }

    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null &&
        lDtUser == 0) {
      //save data User
      dataUser.add({'username': _user, 'email': _email, 'password': _password});
      dataUserXS.write('dataUser', dataUser);

      //navigation
      toast('Daftar Berhasil');
      goHome(_email, _password);
    } else {
      toast('Daftar Gagal');
    }
  }

  void toast(text) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(text),
    ));
  }

  void goHome(email, pass) {
    AuthBlocCubit authBlocCubit = AuthBlocCubit();
    User user = User(
      email: email,
      password: pass,
    );
    authBlocCubit.loginUser(user);
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (_) => BlocProvider(
          create: (context) => HomeBlocCubit()..fetchData(),
          child: HomeBlocScreen(),
        ),
      ),
    );
  }
}
